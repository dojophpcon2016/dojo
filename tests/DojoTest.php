<?php

require __DIR__ . '/../vendor/autoload.php';

/**
 * DojoTest
 *
 * @package CMS
 * @author Maciej Holyszko <mh@ovos.at>
 * @version $Id$
 */
class DojoTest extends \PHPUnit\Framework\TestCase
{
	public function testHelloWorld()
	{
		$game = new \App\Game();
		$output = $game->runCommand('hello');
		$this->assertEquals('This command is unrecognized', $output);
	}

	/**
	 * @dataProvider possibleSteps
	 */
	public function testSteps($step, $answer)
	{
		$game = new \App\Game();
		$output = $game->runCommand($step);
		$this->assertNotEquals($answer, $output);
	}

	public function possibleSteps()
	{
		return [
			['go north', 'This command is unrecognized'],
			['go east', 'This command is unrecognized'],
			['go west', 'This command is unrecognized'],
			['go south', 'This command is unrecognized'],
		];
	}

	public function testWorldExistence()
	{
		$game = new \App\Game();
		$world = $this->getMockBuilder(\App\World::class)
			->getMock();

		$game->setWorld($world);

		$this->assertInstanceOf('App\World', $game->getWorld());
	}

	public function testStartLocation()
	{
		$world = new \App\World();
		$position = $world->getPosition();
		$this->assertEquals([0, 0], $position);
	}

	public function testGoNorth()
	{
		$game = new \App\Game();
		$game->setWorld(new \App\World());
		$answer = $game->runCommand('go north');
		$this->assertEquals('You are at [0,1]', $answer);
	}

	public function testChangePosition()
	{
		$world = new \App\World();
		$world->changePosition(0, 1); // north
		$this->assertEquals([0, 1], $world->getPosition());

		$world->changePosition(-1, 1);
		$this->assertEquals(-1, $world->getX());
		$this->assertEquals(2, $world->getY());

		$world2 = new \App\World();
		$world2->changePosition(1, 0); // east
		$this->assertEquals([1, 0], $world2->getPosition());

	}

}
