<?php

namespace App;

class Game
{
	/**
	 * @var World
	 */
	private $world;

	public function __construct(World $world = null)
	{
		$this->world = null !== $world ? $world : new \App\World();
	}

	public function setWorld(World $world)
	{
		$this->world = $world;
	}

	public function getWorld()
	{
		return $this->world;
	}


	public function runCommand($command)
	{
		$world = $this->getWorld();
		$answer = null;
		switch ($command) {
			case 'go north':
				$world->changePosition(0, 1);
				$answer = 'You are at ['. implode(',', $world->getPosition()) .']';
				break;
			case 'go east':
				$world->changePosition(1, 0);
				$answer = 'You are at ['. implode(',', $world->getPosition()) .']';
				break;
			case 'go west':
				$world->changePosition(-1, 0);
				$answer = 'You are at ['. implode(',', $world->getPosition()) .']';
				break;
			case 'go south':
				$world->changePosition(0, -1);
				$answer = 'You are at ['. implode(',', $world->getPosition()) .']';
				break;
			default:
				$answer = 'This command is unrecognized';
		}

		return $answer;
	}
}