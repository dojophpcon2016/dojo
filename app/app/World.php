<?php
namespace  App;

class World
{
	private $currentLocation;

	public function __construct()
	{
		$this->currentPosition = [0,0];
	}

	public function getPosition()
	{
		return $this->currentPosition;
	}

	public function getX()
	{
		return $this->currentPosition[0];
	}

	public function getY()
	{
		return $this->currentPosition[1];
	}

	public function changePosition($x, $y)
	{
		$this->currentPosition[0] += $x;
		$this->currentPosition[1] += $y;
		return $this;
	}
}